var gulp        = require('gulp');
    sass        = require('gulp-sass');
    browserSync = require('browser-sync').create();


gulp.task('sass', function() {
    return gulp.src(['app/sass/*.scss'])
        .pipe(sass())
        .pipe(gulp.dest("app/css"))
        .pipe(browserSync.stream());
});

gulp.task('serve', ['sass'], function() {
    browserSync.init({
        server: "./app"  
    });

    gulp.watch(['app/sass/*.scss'], ['sass']);
    gulp.watch("app/*.html").on('change', browserSync.reload);
});

gulp.task('default', ['serve']);